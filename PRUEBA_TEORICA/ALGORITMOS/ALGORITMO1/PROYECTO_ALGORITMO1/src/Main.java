import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingrese la cédula ecuatoriana: ");
        String cedula = scanner.nextLine();

        try {
            if (validarCedula(cedula)) {
                System.out.println("La cédula es válida.");
            } else {
                System.out.println("La cédula no es válida.");
            }
        } catch (NumberFormatException e) {
            System.out.println("Error: Ingrese solo números en la cédula.");
        }
    }

    public static boolean validarCedula(String cedula) {
        // Verificar la longitud de la cédula
        if (cedula.length() != 10) {
            return false;
        }

        // Verificar que los primeros dos dígitos estén en el rango permitido
        int dosPrimerosDigitos;
        try {
            dosPrimerosDigitos = Integer.parseInt(cedula.substring(0, 2));
        } catch (NumberFormatException e) {
            // Si se intenta convertir una letra a número, lanzar una excepción
            throw new NumberFormatException("Error: Ingrese solo números en los dos primeros dígitos de la cédula.");
        }
        if (dosPrimerosDigitos < 0 || dosPrimerosDigitos > 24) {
            return false;
        }

        // Verificar que el tercer dígito sea menor a 6
        int tercerDigito = Character.getNumericValue(cedula.charAt(2));
        if (tercerDigito >= 6) {
            return false;
        }

        // Calcular el dígito verificador
        int digitoVerificador = Character.getNumericValue(cedula.charAt(9));
        int suma = 0;

        // Iterar sobre los primeros 9 dígitos de la cédula
        for (int i = 0; i < 9; i++) {
            try {
                int digito = Character.getNumericValue(cedula.charAt(i));
                int coeficiente = (i % 2 == 0) ? 2 : 1;

                // Multiplicar el dígito por el coeficiente
                int resultado = digito * coeficiente;

                // Sumar al total, ajustando si el resultado es mayor a 9
                suma += (resultado > 9) ? resultado - 9 : resultado;
            } catch (NumberFormatException e) {
                // Si se intenta convertir una letra a número, lanzar una excepción
                throw new NumberFormatException("Error: Ingrese solo números en la cédula.");
            }
        }

        // Calcular residuo y determinar el resultado final
        int residuo = suma % 10;
        int resultadoFinal = (residuo == 0) ? 0 : 10 - residuo;

        return (digitoVerificador == resultadoFinal);
    }
}
