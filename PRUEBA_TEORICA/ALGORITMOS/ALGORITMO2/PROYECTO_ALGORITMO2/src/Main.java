import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingrese una palabra: ");
        String palabra = scanner.nextLine();

        try {
            if (esPalindromo(palabra)) {
                System.out.println("La palabra es un palíndromo.");
            } else {
                System.out.println("La palabra no es un palíndromo.");
            }
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }

    public static boolean esPalindromo(String palabra) {
        // Verificar si la palabra es nula o vacía
        if (palabra == null || palabra.isEmpty()) {
            throw new IllegalArgumentException("Error: Ingrese una palabra válida.");
        }

        // Convertir la palabra a minúsculas y eAnaliminar espacios en blanco
        String palabraFormateada = palabra.toLowerCase().replaceAll("\\s", "");

        // Comparar la palabra con su versión invertida
        String palabraInvertida = new StringBuilder(palabraFormateada).reverse().toString();

        return palabraFormateada.equals(palabraInvertida);
    }
}
